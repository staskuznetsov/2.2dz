 #!/bin/bash
git checkout master												 # переключаемся на ветку master
clear 														 # очищаем экран
echo 'Next merged local branches will be deleted (Следующие локальные объединенные ветки будут удалены)'	 # выводим сообщение
git branch --merged | grep -v "dev" | grep -v "master" | grep -v "release" 					 # отображаем все локальные ветки, уже влитые в master, кроме master, release и dev
echo 'Do you want to delete this local branches (Вы хотите удалить эти локальные ветки) (y/n)?'			 # задаем вопрос
read lb														 # считываем ответ
if [ $lb == "y" ] ; then 											 # сравниваем ответ на совпадение с "y"
	git branch --merged | grep -v "dev" | grep -v "master" | grep -v "release" | xargs git branch -d 	 # удаляем все выведенные выше локальные ветки
fi
clear                                                                                                            # очищаем экран
echo 'Next merged remoute branches will be deleted (Следующие удаленные объединенные ветки будут удалены)'	 # выводим сообщение
git branch -r --merged | grep -v "dev" | grep -v "master" | grep -v "release"					 # отображаем все удаленные ветки, уже влитые в master, кроме master, release и dev
echo 'Do you want to delete this local branches (Вы хотите удалить эти удаленные ветки) (y/n)?'			 # задаем вопрос
read rb														 # считываем ответ
if [ $rb == "y" ] ; then											 # сравниваем ответ на совпадение с "y"
	git branch -r --merged | grep -v "dev" | grep -v "master" | grep -v "release"  | xargs git push origin   # удаляем все выведенные выше удаленный ветки
fi
